import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import configureStore from './store/configureStore.dev'
import App from './components/app'
import DevTools from './containers/devTools'
import { Router, Route, Link, browserHistory } from 'react-router'

let store = configureStore();

const devToolsRender = () => {
    if (NODE_ENV === 'development') {
        return <DevTools />
    }
    
};

render(
    <Provider store={store}>
        <div>
            <Router history={browserHistory}>
                <Route path="/" component={App}>
                </Route>
            </Router>
            
            {devToolsRender()}
        </div>
    </Provider>,
    document.getElementById('app')
);