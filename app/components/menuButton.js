import React, { Component, PropTypes } from 'react'
import {Button} from 'react-bootstrap'

const classForActiveButton = (isActive) => {
    return isActive === true ? "active" : ""
};
    
class MenuButton extends Component {
    render() {
        return (
            <div>
                <Button bsClass={"btn btn-sm btn-info" + classForActiveButton(this.props.isActive)}
                        bsSize="small"
                        onClick={this.props.onClick}>
                    {this.props.text}
                </Button>
            </div>
        );
    }
}

MenuButton.propTypes = {
    isActive: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
};
export default MenuButton