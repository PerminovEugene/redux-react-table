import React, { Component, PropTypes } from 'react'
import _ from 'lodash'
import {Button, FormControl, FormGroup, ControlLabel} from 'react-bootstrap'
// import {Typeahead} from 'react-typeahead'

// const rowStates = ["show", "edit"];

class TableRow extends Component {
    
    setStateToEdit() {
        this.props.dispatches.setRowViewType({type: "edit", number: this.props.rowNumber});
    }
    
    onDeleteHandler() {
        const item = {
            id: this.props.data.id,
            url: this.props.config.url
        };
        this.props.dispatches.deleteItem(item);
    }
    
    onSaveHandler() {
        let item = {
            body: Object.assign({}, this.state.editValues),
            data: {
                id: this.props.data.id,
                url: this.props.config.url
            }
        };
        this.props.dispatches.updateItem(item);
    }
    
    onEditElement(event) {
        let key = event.target.name.split('-')[0];
        let newState = Object.assign({}, this.state);
        newState.editValues[key] = event.target.value;
        this.setState(newState);
    }
    
    onEditSelect(event) {
        let key = event.target.name;
        let newState = Object.assign({}, this.state);
        let value = event.target.value;
        if (value === "null") {
            newState.editValues[key] = null;
        } else {
            newState.editValues[key] = value;
        }
        this.setState(newState);
    }
    
    setStateToShow() {
        this.props.dispatches.setRowViewType({type: "show", number: this.props.rowNumber});
    }
     
    getTextTd(data, key) {
        return (<td key={key}>{data}</td>)
    }
    
    getInputTd(data, key, error) {
        let fieldName = key.split('-')[0];
        let value = this.state.editValues[fieldName] || this.state.editValues[fieldName] === "" ? this.state.editValues[fieldName] : this.props.data[fieldName];
        let validationState = null;
        if (error) {
            validationState = "error"
        }
        return(
            <td key={`${key}--1`}>
                <FormGroup controlId="formValidationError1" validationState={validationState}>
                    <FormControl
                        value={value}
                        type="text"
                        placeholder="Enter text"
                        name={key}
                        onChange={this.onEditElement.bind(this)}
                    />
                    <ControlLabel>{error}</ControlLabel>
                </FormGroup>
            </td>);
    }
    
    getAssociatedText(data, key, error, fieldParams) {
        let fieldKey = fieldParams.associatedParams.innerKey;
        if (data) {
            return (<td key={key}>{data[fieldKey]}</td>)
        } else {
            return (<td key={key}> - </td>)
        }
    }
    
    getAutoCompleteTd( data, key, error, fieldParams) {
        let options = [];
        const isSelected = (currentId, selectedIdWrapper) => {
            if (selectedIdWrapper)
                return currentId === selectedIdWrapper.id;
            };
        const isSelectedEmptyOption = (optionData) => {
            return !optionData
        };
        options.push(<option key={`empty-option-key`} value={'null'} selected={isSelectedEmptyOption(data)}> - </option>)
        _.each(this.props.dependencies.autocompleteItems, (value, i) => {
            options.push(<option key={`option-key-${key + i}`} value={value.id} selected={isSelected(value.id, data)}>{value.name}</option>)
        });
        let validationState = null;
        if (error) {
            validationState = "error"
        }
        return (
            <td key={key}>
                <FormGroup controlId="formValidationError1" validationState={validationState}>
                    <FormControl
                        componentClass="select"
                        placeholder="select"
                        name={fieldParams.autocompleteParams.sendKey}
                        onChange={this.onEditSelect.bind(this)}
                    >
                        {options}
                    </FormControl>
                    <ControlLabel>{error}</ControlLabel>
                </FormGroup>
            </td>
        );
    }
    getEditDeleteTdBlock(key) {
        return (
            <td key={`${this.props.rowNumber}-${key}`}>
                <Button bsStyle="info" key={`${key}-1`} onClick={this.setStateToEdit.bind(this)}>Edit</Button>
                <Button bsStyle="danger" key={`${key}-2`} onClick={this.onDeleteHandler.bind(this)}>Delete</Button>
            </td>);
    }
    getSaveCancelTdBlock(key) {
        return (
            <td key={`${this.props.rowNumber}-${key}`}>
                <Button bsStyle="success" key={`${key}-3`} onClick={this.onSaveHandler.bind(this)}>Save</Button>
                <Button bsStyle="warning" key={`${key}-4`} onClick={this.setStateToShow.bind(this)}>Cancel</Button>
            </td>);
    }
    
    constructor(props) {
        super(props);
        this.state = {editValues: {}};
        this.tdTypes = {
            "text": (data, key) => {
                return this.getTextTd(data, key);
            },
            "input": (data, key, error) => {
                return this.getInputTd(data, key, error);
            },
            "associated-text": (data, key, error, fieldParams) => {
                return this.getAssociatedText(data, key, error, fieldParams);
            },
            "autocomplete": (data, key, error, fieldParams) => {
                return this.getAutoCompleteTd(data, key, error, fieldParams);
            },
            "edit-delete": (key) => {
                return this.getEditDeleteTdBlock(key);
            },
            "save-cancel": (key) => {
                return this.getSaveCancelTdBlock(key);
            }
        };
    }
    
    getColumns (data, config, rowViewType, rowNumber) {
        let result = [];
        //add columns for fields
        _.each(config.fields, (fieldParams, key) => {
            let tdData = data[key];
            let tdTypeOfView = fieldParams[rowViewType];
            let getTd = this.tdTypes[tdTypeOfView];
            let error;
            if (data.errors) {
                error = data.errors[key];
            }
            if (getTd) {
                let tdKey = `${key}-${rowNumber}`;
                result.push(getTd(tdData, tdKey, error, fieldParams));
            }
        });
        
        //add columns for buttons
        _.each(config.specialZone, (zoneParams, zoneName) => {
            let currentZoneView = zoneParams[this.props.data.viewState];
            let getTd = this.tdTypes[currentZoneView];
            if (getTd) {
                let tdKey = `${zoneName}-${rowNumber}`;
                result.push(getTd(tdKey));
            }
        });
        return result
    }
    
    render() {
        return (
            <tr key={`table-row-${this.props.rowNumber}`}>
                {this.getColumns(this.props.data, this.props.config, this.props.data.viewState, this.props.rowNumber)}
            </tr>
        );
    }
}

TableRow.propTypes = {
    data: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired,
    rowNumber: PropTypes.number.isRequired,
    dispatches: PropTypes.object.isRequired,
};
export default TableRow