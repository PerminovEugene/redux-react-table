import React, { Component, PropTypes } from 'react'
import _ from 'lodash'
import {Button} from 'react-bootstrap'
import {FormControl, FormGroup, ControlLabel} from 'react-bootstrap'

// const rowStates = ["show", "edit"];

class TableRow extends Component {
    
    onSaveHandler() {
        let item = {
            body: this.props.newItemData,
            url: this.props.config.url
        };
        this.props.dispatches.createNewItem(item);
    }
    
    onEditElement(event) {
        this.props.dispatches.setCreateItemRowField({key: event.target.name, value: event.target.value});
    }
    
    onEditSelect(event) {
        let value;
        if (event.target.value === "null") {
            value = null;
        } else {
            value = event.target.value;
        }
        this.props.dispatches.setCreateItemRowField({key: event.target.name, value: value});
    }
    
    constructor(props) {
        super(props);
        this.state = {editValues: {}};
        
        this.tdTypes = {
            "input": (data, fieldName, error) => {
               return this.getInput(data, fieldName, error)
            },
            "autocomplete": (data, key, error, fieldParams) => {
                return this.getAutoComplete(data, key, error, fieldParams);
            },
            "save-cancel": (key) => {
                return this.getSaveCancelBlock(key);
            }
        };
    }
    getInput(data, fieldName, error) {
        let value =  this.props.newItemData[fieldName] ? this.props.newItemData[fieldName] : "";
        let validationState = null;
        if (error) {
            validationState = "error"
        }
        return (
            <td key={`${fieldName}--1`}>
                <FormGroup controlId="formValidationError1" validationState={validationState}>
                <FormControl
                type="text"
                value={value}
                placeholder="Enter text"
                name={fieldName}
                onChange={this.onEditElement.bind(this)}
                />
                <ControlLabel>{error}</ControlLabel>
                </FormGroup>
            </td>
        )
    }
    
    getAutoComplete(data, key, error, fieldParams) {
        let options = [];
        const isSelected = (currentId, selectedIdWrapper) => {
            if (selectedIdWrapper)
                return currentId === selectedIdWrapper.id;
        };
        const isSelectedEmptyOption = (optionData) => {
            return !optionData
        };
        options.push(<option value={'null'} selected={isSelectedEmptyOption(data)}> - </option>);
        _.each(this.props.dependencies.autocompleteItems, (value, i) => {
            options.push(<option value={value.id} selected={isSelected(value.id, data)}>{value.name}</option>)
        });
        let validationState = null;
        if (error) {
            validationState = "error"
        }
        return (
            <td>
                <FormGroup controlId="formValidationError1" validationState={validationState}>
                    <FormControl
                        componentClass="select"
                        placeholder="select"
                        name={fieldParams.autocompleteParams.sendKey}
                        onChange={this.onEditSelect.bind(this)}
                    >
                        {options}
                    </FormControl>
                    <ControlLabel>{error}</ControlLabel>
                </FormGroup>
            </td>
        );
    }
    
    getSaveCancelBlock(key) {
        return (
            <td>
                <Button bsStyle="success" key={`${key}-3`} onClick={this.onSaveHandler.bind(this)}>Add</Button>
            </td>)
    }
    
    getColumns (config, rowViewType, newItemData) {
        let result = [];
        
        //add columns for fields
        _.each(config.fields, (fieldParams, fieldName) => {
            let tdData = newItemData[fieldName];
            let tdTypeOfView = fieldParams[rowViewType];
            let getTd = this.tdTypes[tdTypeOfView];
            let error;
            if (newItemData.errors) {
                error = newItemData.errors[fieldName];
            }
            if (getTd) {
                result.push(getTd(tdData, fieldName, error, fieldParams));
            }
        });
        
        //add columns for buttons
        _.each(config.specialZone, (zoneParams, zoneName) => {
            let currentZoneView = zoneParams[rowViewType];
            let getTd = this.tdTypes[currentZoneView];
            if (getTd) {
                let tdKey = `${zoneName}-${-1}`;
                result.push(getTd(tdKey));
            }
        });
        return result
    }
    
    render() {
        return (
            <tr>
                {this.getColumns(this.props.config, "edit", this.props.newItemData)}
            </tr>
        );
    }
}

TableRow.propTypes = {
    newItemData: PropTypes.object.isRequired,
    config: PropTypes.object.isRequired,
    dispatches: PropTypes.object.isRequired
};
export default TableRow