import React from 'react'
import LeftMenu from '../containers/leftMenu'
import Content from './../containers/content'
import {Col} from 'react-bootstrap'

const App = () => (
    
    <div>
        <div className="container">
            <Col xs={3} md={3}>
                <LeftMenu />
            </Col>
            <Col xs={9} md={9}>
                <Content />
            </Col>
        </div>
    </div>
);

export default App