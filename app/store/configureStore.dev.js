import { createStore, applyMiddleware, compose } from 'redux';
import { persistState } from 'redux-devtools';
import createSagaMiddleware from 'redux-saga'
import thunk from 'redux-thunk';
import rootReducer from '../reducers';
import DevTools from './../containers/devTools';
import tableCrudSaga from './../sagas/table';

const sagaMiddleware = createSagaMiddleware();

let enhancer;
if (NODE_ENV === 'development') {
    enhancer = compose(
        applyMiddleware(thunk),
        applyMiddleware(sagaMiddleware),
    
        
        DevTools.instrument(),
        persistState(
            window.location.href.match(
                /[?&]debug_session=([^&#]+)\b/
            )
        )
    );
} else {
    enhancer = compose(
        applyMiddleware(thunk),
        applyMiddleware(sagaMiddleware),
        
        persistState(
            window.location.href.match(
                /[?&]debug_session=([^&#]+)\b/
            )
        )
    );
}


export default function configureStore(initialState) {
    const store = createStore(rootReducer, initialState, enhancer );
    sagaMiddleware.run(tableCrudSaga);
    
    if (module.hot) {
        module.hot.accept('../reducers', () =>
            store.replaceReducer(require('../reducers').default)
        );
    }
    
    return store;
}