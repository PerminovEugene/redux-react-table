import { call, put, takeEvery, takeLatest } from 'redux-saga/effects'
require('whatwg-fetch')



const fetchData = (config) => {
    return fetch(`${API_URL}/${config.path}`,
        {
            headers: new Headers({
                'Content-Type': 'application/json',
                Accept: 'application/json',
            }),
            method: config.method,
            body: JSON.stringify(config.body)
        })
        .then((response) => {
            return response.json();
        })
        .catch((err) => {
            console.log(err);
            put({type: "USER_FETCH_FAILED", message: err});
        });
};

const fetchSomeData = (configs) => {
    let promises = [];
    _.each(configs, (config) => {
        promises.push(new Promise((resolve, reject) => {
            fetch(`${API_URL}/${config.path}`,
                {
                    headers: new Headers({
                        'Content-Type': 'application/json',
                        Accept: 'application/json',
                    }),
                    method: config.method,
                    body: JSON.stringify(config.body)
                })
                .then((response) => {
                    return response.json();
                })
                .then((json) => {
                    resolve({ data: json.data, tableName: config.tableName});
                })
                .catch((err) => {
                    reject(err);
                })
            })
        )
    });
    return Promise.all(promises)
        .then((results) => {
            console.log('RETURN: ', results)
            return results
        })
        .catch((err) => {
            put({type: "USER_FETCH_FAILED", message: err});
        });
};


function* fetchTableData(action) {
    try {
        let dependencies = action.configsTables[action.tableName].dependencies;
        let configs = [];
        configs.push({
            type: "START_FETCH",
            tableName: action.tableName,
            path: action.configsTables[action.tableName].url,
            method: "GET"
        });
        _.each(dependencies, (dependTableName, key) => {
            configs.push({
                type: "START_FETCH",
                tableName: dependTableName,
                path: action.configsTables[dependTableName].url,
                method: "GET"
            });
        });
        const response = yield call(fetchSomeData, configs);
        console.log('>>>', response)
        yield put({
            type: "LOADED_TABLE_DATA",
            payload: {tablesData: response},
            tableName: action.tableName
        });
    } catch (e) {
        yield put({type: "DATA_FETCH_FAILED", message: e.message});
    }
};

// function* fetchEmployeeData(action) {
//     const config = {
//         type: "START_FETCH",
//         path: "employee",
//         method: "GET"
//     };
//     try {
//         const response = yield call(fetchData, config);
//         yield put({type: "LOADED_EMPLOYEE_DATA", payload: response.data});
//     } catch (e) {
//         yield put({type: "DATA_FETCH_FAILED", message: e.message});
//     }
// };

function* createItem(action) {
    const config = {
        type: "START_FETCH",
        path: `${action.payload.url}`,
        method: "PUT",
        body: action.payload.body
    };
    try {
        const response = yield call(fetchData, config);
        if (response.errors) {
            yield put({type: "CREATE_ITEM_ERROR", payload: response});
        } else {
            yield put({type: "CREATED_ITEM", payload: response});
        }
    } catch (e) {
        yield put({type: "DATA_FETCH_FAILED", message: e.message});
    }
}

function* updateItem(action) {
    const config = {
        type: "START_FETCH",
        path: `${action.item.data.url}/${action.item.data.id}`,
        method: "POST",
        body: action.item.body
    };
    try {
        const response = yield call(fetchData, config);
        if (response.errors) {
            yield put({type: "UPDATE_ITEM_ERROR", payload: response, itemId: action.item.data.id});
        } else {
            yield put({type: "UPDATED_ITEM", payload: response});
        }
    } catch (e) {
        yield put({type: "DATA_FETCH_FAILED", message: e.message});
    }
}

function* deleteItem(action) {
    const config = {
        type: "START_FETCH",
        path: `${action.item.url}/${action.item.id}`,
        method: "DELETE"
    };
    try {
        let response = yield call(fetchData, config);
        if (response.errors) {
            yield put({type: "DELETE_ITEM_ERROR", payload: response, itemId: action.item.id});
        } else {
            yield put({type: "DELETED_ITEM", payload: action});
        }
    } catch (e) {
        yield put({type: "DATA_FETCH_FAILED", message: e.message});
    }
}

function* mySaga() {
    yield takeEvery("SWITCH_TABLE", fetchTableData);
    yield takeEvery("CREATE_ITEM", createItem);
    yield takeEvery("UPDATE_ITEM", updateItem);
    yield takeEvery("DELETE_ITEM", deleteItem);
};

export default mySaga;