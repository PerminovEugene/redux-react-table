import { connect } from 'react-redux'
import { table } from '../actions/table'
import React, { Component } from 'react'
import MenuButton from '../components/menuButton'
import { switchTable } from '../actions/table'
import config from './../configs/tables.json'
import _ from 'lodash'

const mapStateToProps = (state) => {
    return {
        activeTable: state.table.activeTable
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        switchTable: (newActiveTableName, configs) => dispatch(switchTable(newActiveTableName, configs))
    }
};

class leftMenuContainer extends Component  {
    Super() {}
    
    renderLiWithButtons() {
        let items = [];
        _.each(config.tables, (tableData, tableName) => {
            items.push(<li key={`li-key-${tableName}`}>
                <MenuButton
                    key={`menu-button-${tableName}`}
                    onClick={this.props.switchTable.bind(this, tableName, config.tables)}
                    isActive={this.props.activeTable === tableName}
                    text={tableName}
                />
            </li>)
        });
        return items;
    }
    
    render() {
        return (
            <div className="well sidebar-nav" key={'left-menu-key'}>
                <ul className="nav nav-list">
                    <li className="nav-header"> Tables </li>
                    {this.renderLiWithButtons()}
                </ul>
            </div>
        )
    }
};

const LeftMenu = connect(
    mapStateToProps,
    mapDispatchToProps
)(leftMenuContainer);

export default LeftMenu