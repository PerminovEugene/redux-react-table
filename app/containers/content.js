import { connect } from 'react-redux'
import { table } from '../actions/table'
import React, { Component } from 'react'
import {Table} from 'react-bootstrap'
import fullConfig from './../configs/tables.json'
import _ from 'lodash'
import TableRowCreateNewItem from './../components/TableRowCreateNewItem'
import TableRow from './../components/tableRow'
import { updateItem, deleteItem, setRowViewType, createNewItem, setCreateItemRowField } from '../actions/table'

const conf = fullConfig.tables;

const mapStateToProps = (state) => {
    return {
        activeTable: state.table.activeTable,
        Department: state.table.Department,
        Employee: state.table.Employee,
        newItemData: state.table.newItemData
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        updateItem: (item) => dispatch(updateItem(item)),
        deleteItem: (item) => dispatch(deleteItem(item)),
        setRowViewType: (item) => dispatch(setRowViewType(item)),
        createNewItem: (item) => dispatch(createNewItem(item)),
        setCreateItemRowField: (item) => dispatch(setCreateItemRowField(item))
    }
};

const getHeaderTd = (tableConfig) => {
    if (!tableConfig) {
        return (
            <td> Please select table </td>
        )
    }
    let result = [];
    _.each(tableConfig.fields, (config, key) => {
        result.push((<td key={`table-header-${key}`}> {config.header} </td>))
    });
    return result;
};

const getTableBody = (props) => {
    const config = conf[props.activeTable]
        , activeTable = props.activeTable
        , data = props[activeTable]
        , itemsDispatches = {
            updateItem: props.updateItem,
            deleteItem: props.deleteItem,
            setRowViewType: props.setRowViewType
        }
        , newRowDispatches = {
            setCreateItemRowField: props.setCreateItemRowField,
            createNewItem: props.createNewItem
        }
        , newItemData = props.newItemData;
    
    
    if (!data) {
        return (
            <tr></tr>
        )
    }
    let result = [];
    
    let dependencies ={};
    if (config.dependencies) {
        _.each(config.dependencies, (tableName, key) => {
            if (props[tableName]) {
                dependencies[key] = props[tableName];
            } else {
                console.log('need load data')
            }
        });
    }
    // first line in table - for create new item
    result.push(
        <TableRowCreateNewItem
            key={`table-body-new-row`}
            config={config}
            newItemData={newItemData}
            dispatches={newRowDispatches}
            dependencies={dependencies}
        />
    );
    // other lines
    _.each(data, (trData, index) => {
        result.push(
            <TableRow
                key={`table-body-row-${index}`}
                data={trData}
                config={config}
                rowNumber={index}
                dispatches={itemsDispatches}
                dependencies={dependencies}
            />)
    });
    return result;
};

class ContentContainer extends Component  {
    Super() {}
    
    render() {
        const props = this.props;
        const tableConfig = conf[props.activeTable];
        return (
            <div>
                <Table responsive>
                    <thead>
                        <tr>
                            {getHeaderTd(tableConfig)}
                        </tr>
                    </thead>
                    <tbody>
                        {getTableBody(props)}
                    </tbody>
                </Table>
            </div>
        )
    }
};

const VisibleTodoList = connect(
    mapStateToProps,
    mapDispatchToProps
)(ContentContainer);

export default VisibleTodoList