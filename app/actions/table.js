export const setRowViewType = (data) => {
    console.log('hey')
    return {
        type: "SET_ROW_VIEW",
        payload: data
    }
};

export const setCreateItemRowField = (data) => {
    return {
        type: "SET_CREATE_ITEM_ROW_FIELD",
        payload: data
    }
};

export const createNewItem = (data) => {
    return {
        type: "CREATE_ITEM",
        payload: data
    }
};

export const updateItem = (item) => {
    return {
        type: "UPDATE_ITEM",
        item: item
    }
};

export const deleteItem = (item) => {
    return {
        type: "DELETE_ITEM",
        item: item
    }
};

export const switchTable = (tableName, configsTables) => {
    return {
        type: 'SWITCH_TABLE',
        tableName: tableName,
        configsTables: configsTables
    }
};