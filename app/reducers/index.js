import { combineReducers } from 'redux'
import table from './table'

const todoApp = combineReducers({
    table
});

export default todoApp