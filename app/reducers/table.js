import _ from 'lodash'

const table = (state = 'EMPTY', action) => {
    let newState = {}, id, activeTable, index;
    if (state !== 'EMPTY') {
        newState = _.cloneDeep(state);
    }
    if (!newState.newItemData) {
        newState.newItemData = {}
    }
    
    switch (action.type) {
        case 'LOADED_TABLE_DATA':
            let tableName = action.tableName;
            newState.newItemData.errors = {};
            _.each(action.payload.tablesData, (tableData) => {
                newState[tableData.tableName] = tableData.data;
                _.each(newState[tableData.tableName],  (value) => {
                    value.viewState = "show";
                    value.errors = {};
                });
            });
            newState.activeTable = tableName;
            return newState;

        case 'SET_CREATE_ITEM_ROW_FIELD':
            newState.newItemData[action.payload.key] = action.payload.value;
            return newState;
    
        case 'CREATED_ITEM':
            activeTable = newState.activeTable;
            newState.newItemData = {};
            newState[activeTable].push(Object.assign({}, action.payload.data, {viewState: "show"}));
            return newState;
    
        case 'CREATE_ITEM_ERROR':
            newState.newItemData.errors = action.payload.errors;
            return newState;

        case 'DELETED_ITEM':
            id = action.payload.item.id;
            activeTable = newState.activeTable;
            
            index = _.findIndex(newState[activeTable], (obj) => {
                return obj.id === id;
            });
            newState[activeTable].splice(index, 1);
            return newState;
    
        
        case 'UPDATED_ITEM':
            id = action.payload.data.id;
            activeTable = newState.activeTable;
            index = _.findIndex(newState[activeTable], (obj) => {
                return obj.id === id;
            });
            newState[activeTable][index].errors = {};
            newState[activeTable][index] = action.payload.data;
            newState[activeTable][index].viewState = "show";
            return newState;
        
        case 'UPDATE_ITEM_ERROR':
            id = action.itemId;
            activeTable = newState.activeTable;
            index = _.findIndex(newState[activeTable], (obj) => {
                return obj.id === id;
            });
            newState[activeTable][index].errors = action.payload.errors;
            return newState;
        
        
        case 'SET_ROW_VIEW':
            activeTable = newState.activeTable;
            newState[activeTable][action.payload.number].viewState = action.payload.type;
            return newState;

        default:
            return state
    }
}

export default table