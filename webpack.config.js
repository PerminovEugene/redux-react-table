let path = require('path');
let webpack = require('webpack');
const NODE_ENV = process.env.NODE_ENV || 'development';
const API_URL  = process.env.API_URL || 'http://localhost:3000';

console.log(API_URL);

module.exports = {
    entry: './app/index.js',
    output: {
        filename: 'bundle.js',
        library: "home",
        path: path.resolve(__dirname, 'dist')
    },
    watch: NODE_ENV === 'development',
    watchOptions: {
        aggregateTimeout: 100
    },
    
    devtool: NODE_ENV === 'development' ? "source-map" : false,
    
    module: {
        loaders: [{
            test: /\.js$/,
            loader: "babel-loader",
            exclude: /node_modules/,
            query: {
                plugins: ['transform-runtime'],
                presets: ['es2015', 'stage-0', 'react'],
            }
        }]
    },
    
    plugins: [
        new webpack.DefinePlugin({
            NODE_ENV: JSON.stringify(NODE_ENV),
            API_URL:  JSON.stringify(API_URL)
        })
    ]
};

if (NODE_ENV === 'production') {
    module.exports.plugins.push(
        new webpack.optimize.UglifyJsPlugin({
            compress: {
                warnings: false,
                drop_console: true,
                unsafe: true
            }
        })
    )
}